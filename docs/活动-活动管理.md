# 5  **活动管理**

> 活动管理分为五块内容：优惠券、老带新活动、推荐有礼活动、限时立减、组合打折。
>
> **限购课程不得参与活动**。

## 5.1 优惠券 

> 优惠券中包含：新增优惠券、发放优惠券、编辑、发布、下架、查看领取/发放记录、作废用户已领取优惠券等操作。

优惠券界面如下：

![优惠券01](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/capon01.png)

优惠券领取记录界面：

![优惠券领取记录界面](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/capon02.png)



## 5.2 老带新活动

> 活动设置立减金额，用户下单可减免对应的金额。活动发布前需要先在活动详情页中设置课程，修改立减金额价格。老带新、推荐有礼、限时立减三种活动已发布状态的课程不可重复。
>
> 玩法：
>
> 1.老学员下单，将课程分享给新用户，新用户根据链接去下单，填写老学员手机号可享受活动的立减金额优惠，订单确认后系统返给老学员积分。



老带新活动界面如下：

![老带新活动](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/oldRecommend01.png)



设置活动课程：点击右侧的 ‘课程’ 列，然后在弹窗中点击添加课程即可。

![老带新添加课程](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/addOldRecommendActivityCourse01.png)

## 5.3 推荐有礼活动

> 活动设置立减金额，用户下单可减免对应的金额。活动发布前需要先在活动详情页中设置课程，修改立减金额价格。老带新、推荐有礼、限时立减三种活动已发布状态的课程不可重复。
>
> 玩法：1.推荐人将课程分享给其他用户（推荐人购买了课程），其他用户根据链接去下单可享受活动的立减金额优惠，订单确认后系统返给推荐人活动的 ‘下单分享’ 积分。
>
> 2.推荐人推荐课程给其他用户（推荐人未购买课程），其他用户根据链接下单享受活动的立减金额优惠，订单确认后系统返给推荐人活动的 ‘未下单分享’ 积分。



推荐有礼活动界面如下：

![推荐有礼活动界面](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/newRecommend01.png)



设置活动课程：

![](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/addRecommendRewardActivityCourse01.png)



## 5.4 限时立减活动

> 活动设置立减金额，用户下单可减免对应的金额。活动发布前需要先在活动详情页中设置课程，修改立减金额价格。老带新、推荐有礼、限时立减三种活动已发布状态的课程不可重复。

限时立减活动界面如下：

![限时立减活动界面](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/timeLimitedSpecial01.png)





## 5.5 组合打折活动

> 组合打折活动可以与老带新和推荐有礼通过立减金额方式的活动同时参与。活动优惠方式如下：
>
> 设置课程参与的活动方式，有默认、老带新和推荐有礼三种方式。
>
> ​		默认：填入活动折扣百分比或者活动价格。
>
> ​		老带新：填入活动折扣百分比或者活动价格，额外可选择设置 **立减金额（新学员下单立减）** 和 **下单奖励积分（奖励老学员）**。
>
> ​		推荐有礼：填入活动折扣百分比或者活动价格，额外可选择设置 **立减金额（被推荐学员下单立减）** 和 **下单奖励积分（奖励推荐人）**。
>
> **注意：用户需要扫描活动二维码参与活动，且老带新的新学员必须为 新注册 或者 非在读状态超过180天 的记录。**
>
> 生成的活动发布之后会有二维码产生，点击活动列表的下单码，即可显示二维码。

![](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/groupActivityList01.png)



### 5.5.1 活动设置

我们提供了两种设置方式，**第一种**是在组合打折页面点击 ‘新增活动’ 设置，**第二种**是在 ‘课程管理’下的 ‘课程报名’ 界面勾选上要设置的课程报名，然后点击 ‘快速生成下单二维码’ 按钮，填写好活动信息，即可添加活动（**注意：活动默认直接发布，只能通过下架操作移除**）。第一种界面如下：

![添加组合活动01](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/addGroupActivty03.png)

第二种界面如下：

![快速生成下单二维码01](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/addGroupActivty01.png)

![快速生成下单二维码02](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/addGroupActivity02.png)



**生成之后，系统回将生成的二维码弹出，‘不保存’ 活动的方式，请将二维码截图保存（请勿拍照，二维码会有识别错误的可能），或者右击图片，另存到本地。‘保存’ 的方式，可在组合打折活动页面点击图片查看二维码。用户扫描二维码即可参与活动。**

**支付宝码由于第三方的特殊性，请私发给用户或者让用户直接扫描。**



### 5.5.2 设置活动课程

活动设置课程的方式目前有3种，

1. 生成快速下单二维码处设置（参考：[5.5.1 活动设置](https://boao-doc.gitee.io/op-help/#/./docs/%E6%B4%BB%E5%8A%A8-%E6%B4%BB%E5%8A%A8%E7%AE%A1%E7%90%86?id=_551-%e6%b4%bb%e5%8a%a8%e8%ae%be%e7%bd%ae)）。
2. 添加组合活动处设置（参考：[5.5.1 活动设置](https://boao-doc.gitee.io/op-help/#/./docs/%E6%B4%BB%E5%8A%A8-%E6%B4%BB%E5%8A%A8%E7%AE%A1%E7%90%86?id=_551-%e6%b4%bb%e5%8a%a8%e8%ae%be%e7%bd%ae)）。
3. 组合活动列表操作列的课程列表按钮设置。

![活动课程管理](https://yjp-public-file.oss-cn-beijing.aliyuncs.com/opDocImg/groupActivityCourse.png)